<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    
    public function kirim(Request $request)
    {
        $nama1 = $request['depan'];
        $nama2 = $request['belakang'];

        return view('halaman.welcome', ['depan' => $nama1, 'belakang' => $nama2]);

    }

}
